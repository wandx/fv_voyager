let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
    "resources/assets/css/bootstrap.min.css",
    "resources/assets/css/font-awesome.min.css",
    "resources/assets/css/font-awesome.min.css",
    "resources/assets/lib/slick/slick.css",
    "resources/assets/lib/slick/slick-theme.css",
    "resources/assets/lib/select2/css/select2.css",
    "resources/assets/css/main.css",
    "resources/assets/css/color2.css",
    "resources/assets/css/responsive.css",
    "resources/assets/css/wandx.css",
],"public/css/app.css");

mix.copyDirectory("resources/assets/fonts","public/fonts");
mix.copyDirectory("resources/assets/lib/slick/fonts","public/fonts");
mix.copyDirectory("resources/assets/img","public/img");

mix.scripts([
    "resources/assets/js/jquery.min.js",
    "resources/assets/js/counter.js",
    "resources/assets/js/bootstrap.min.js",
    "resources/assets/lib/slick/slick.js",
    "resources/assets/lib/select2/js/select2.full.min.js",
    "resources/assets/js/custom.js"
],"public/js/app.js");
