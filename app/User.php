<?php

namespace App;

use App\Models\Company;
use App\Models\Demand;
use App\Models\Offer;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Storage;
use TCG\Voyager\Facades\Voyager;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','is_admin','skill','avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $appends = [
        "avatar_url"
    ];

    public function demands(){
        return $this->hasMany(Demand::class);
    }

    public function offers(){
        return $this->hasMany(Offer::class);
    }

    public function company(){
        return $this->hasOne(Company::class);
    }

    public function getAvatarUrlAttribute(){
        $url = "http://placehold.it/100";

        if(Storage::exists($this->avatar)){
            $url = Storage::url($this->avatar);
        }else{
            $url = Voyager::image($this->avatar,$url);
        }

        return $url;
    }


    protected static function boot()
    {
        parent::boot();
        self::creating(function($model){
            if(auth()->user()->is_admin == 1){
                $model->is_admin = 1;
            }
        });
    }
}
