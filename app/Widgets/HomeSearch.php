<?php

namespace App\Widgets;

use App\Models\Country;
use Arrilot\Widgets\AbstractWidget;

class HomeSearch extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $country = (new Country())->newQuery()->pluck("name","id");

        return view('widgets.home_search', [
            "country" => $country
        ]);
    }
}
