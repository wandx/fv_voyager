<?php

namespace App\Widgets;

use App\Models\Partner;
use Arrilot\Widgets\AbstractWidget;

class Partners extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $partners = (new Partner())->get();

        return view('widgets.partners', [
            "partners" => $partners
        ]);
    }
}
