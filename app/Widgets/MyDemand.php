<?php

namespace App\Widgets;

use App\Models\Demand;
use Arrilot\Widgets\AbstractWidget;

class MyDemand extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $demand = (new Demand())->whereUserId(auth()->user()->id)->paginate(10);

        return view('widgets.my_demand', [
            "demands" => $demand
        ]);
    }
}
