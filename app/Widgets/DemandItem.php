<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

class DemandItem extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        "logo" => null,
        "company_name" => null,
        "city_name" => null,
        "title" => "null",
        "min_price" => 0,
        "max_price" => 0,
        "id" => null
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        return view('widgets.demand_item', [
            'config' => $this->config,
        ]);
    }
}
