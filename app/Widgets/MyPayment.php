<?php

namespace App\Widgets;

use App\Models\Order;
use Arrilot\Widgets\AbstractWidget;

class MyPayment extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $transaction = (new Order())->newQuery()->whereUserId(auth()->user()->id)->latest()->paginate(10);


        return view('widgets.my_payment', [
            'transactions' => $transaction,
        ]);
    }
}
