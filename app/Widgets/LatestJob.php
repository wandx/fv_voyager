<?php

namespace App\Widgets;

use App\Models\Demand;
use Arrilot\Widgets\AbstractWidget;

class LatestJob extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $x = (new Demand())->newQuery()->where("status","published")->latest()->take(5)->get();

        return view('widgets.latest_job', [
            "demands" => $x
        ]);
    }
}
