<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

class DashboardNav extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        "active" => "profile-information",
        "list" => [
            [
                "url" => "/dashboard/userinfo",
                "text" => "Profile Information",
                "slug" => "profile-information"
            ],
            [
                "url" => "/dashboard/my-demand",
                "text" => "My Demand",
                "slug" => "my-demand"
            ],
            [
                "url" => "/dashboard/my-offer",
                "text" => "My Offer",
                "slug" => "my-offer"
            ],
            [
                "url" => "/dashboard/my-company",
                "text" => "My Company",
                "slug" => "my-company"
            ],
            [
                "url" => "/dashboard/my-message",
                "text" => "My Message",
                "slug" => "my-message"
            ],
            [
                "url" => "/dashboard/my-payment",
                "text" => "Payment History",
                "slug" => "my-payment"
            ],
            [
                "url" => "#",
                "text" => "Account Setting",
                "slug" => "account-setting"
            ],
            [
                "url" => "/logout",
                "text" => "Logout",
                "slug" => "logout"
            ]
        ]
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        return view('widgets.dashboard_nav', [
            'config' => $this->config,
        ]);
    }
}
