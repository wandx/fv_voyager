<?php

namespace App\Widgets;

use App\Models\Offer;
use Arrilot\Widgets\AbstractWidget;

class MyOffer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $offers = (new Offer())->where("status","published")->where("user_id",auth()->user()->id)->latest()->paginate(10);

        return view('widgets.my_offer', [
            "offers"=>$offers,
        ]);
    }
}
