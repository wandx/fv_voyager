<?php

namespace App\Http\Controllers\Site;

use App\Models\Demand;
use App\Models\Offer;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DemandCont extends Controller
{
    public function index(Request $request,Demand $demand){
        $demand = $demand->newQuery();

        $wheres = function($q) use ($request){
            // main condition
            $q->whereStatus("published");
            // TODO: show paid demand only

            // optional condition
            if($request->has("keywords")){
                $title = $request->input("keywords");
                $q->where("title","like","%".$title."%");

                foreach ($this->buildKwd($request->input("keywords")) as $kwd){
                    $q->orWhere("description","like","%".$kwd."%");
                }
            }

            if($request->has("country")){
                $country = $request->input("country");
                $q->where("country_id",$country);
            }
        };

        $demands = $demand->where($wheres)->paginate(10)->appends($request->query());
        return view("demand.index",compact("demands","request"));
    }

    public function single($id,Demand $demand){
        $demand = $demand->findOrFail($id);
        return view("demand.single",compact("demand"));
    }

    public function submit_offer($id,Request $request,Offer $offer,Order $order){
        // prevent submit offer to own demand
        if($id == auth()->user()->id){
            abort(404);
        }

        // validate input
        $validator = app("validator")->make($request->all(),[
            "title" => "required",
            "description" => "required",
            "price" => "required"
        ]);

        // if validation error, return with message
        if($validator->fails()){
            return back()->withErrors($validator->messages());
        }

        if($request->hasFile("attachment_file")){
            $name = "offer_".Carbon::now()->format("dmYHis").".pdf";
            $request->file("attachment_file")->storeAs("public/attachment/",$name);
            $request->merge(["doc"=>"public/attachment/".$name]);
        }

        $request->merge(["demand_id"=>$id]);
        $request->merge(["status"=>"pending"]);
        $request->merge(["user_id"=>auth()->user()->id]);

        $store = $offer->create($request->except("_token","attachment_file"));
        $order->newQuery()->create([
            "invoice_no" => "OFR".rand(1000000,9999999),
            "payment_for" => "offer",
            "amount" => setting("site.base_price",10),
            "description" => "Payment for giving offer ".$store->title,
            "status" => "pending",
            "related_id" => $store->id
        ]);

        // TODO: Redirect to payment page
        return redirect("dashboard")->with(["success"=>"Waiting for payment"]);
    }

    private function buildKwd($str){
        $ex = explode(" ",$str);
        return $ex;
    }
}
