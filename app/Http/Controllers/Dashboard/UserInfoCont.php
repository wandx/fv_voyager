<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;

class UserInfoCont extends Controller
{
    public function index(){
        return view("dashboard.user_info.index");
    }

    public function update(Request $request){
        if($request->hasFile("avatar_file")){
            $path = "users/avatar/";
            $name = str_random(10).".".$request->file("avatar_file")->getClientOriginalExtension();
            Storage::delete(auth()->user()->avatar);
            $request->file("avatar_file")->storeAs("public/".$path,$name);
            $request->merge(["avatar"=>$path.$name]);
        }

        auth()->user()->update($request->only("skill","avatar","name"));
        return back()->with(["success"=>"Updated"]);

    }
}
