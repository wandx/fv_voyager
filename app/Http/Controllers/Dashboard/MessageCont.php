<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Message;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MessageCont extends Controller
{
    public function index(Message $message){
        $messages = $message->newQuery()->where("receiver_id",auth()->user()->id)->orWhere("sender_id",auth()->user()->id)->latest()->get();
        return view("dashboard.mymessage.index",compact("messages"));
    }

    public function send_message(Request $request,Message $message){
        $receiver = $request->input("receiver_id");
        $body = $request->input("body");

        DB::beginTransaction();
        try{
            $message = $message->newQuery()->create([
                "sender_id" => auth()->user()->id,
                "receiver_id" => $receiver
            ]);

            $message->message_items()->create([
                "user_id" => auth()->user()->id,
                "body" => $body
            ]);
            DB::commit();
            return back()->with(["success"=>"Message Sent"]);
        }catch (\Exception $exception){
            DB::rollBack();
            return back()->withErrors(["failed"=>"Message not send"]);
        }
    }

    public function reply($message_id,Request $request,Message $message){
        $message = $message->newQuery()->findOrFail($message_id);

        $message->message_items()->create([
            "user_id" => auth()->user()->id,
            "body" => $request->input("body")
        ]);
        return back()->with(["success"=>"Message Sent"]);
    }

    public function show($message_id,Message $message){
        $messages = $message->newQuery()->findOrFail($message_id);
        if($messages->message_items()->count() > 0){
            $set = $messages->message_items()->where("user_id","!=",auth()->user()->id)->update(["is_read"=>1]);
        }
        $messages = $messages->message_items()->orderBy("created_at","asc")->get();
//        return $messages;
        return view("dashboard.mymessage.show",compact("messages","message_id"));
    }
}
