<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentCont extends Controller
{
    public function index(){
        return view("dashboard.payment.index");
    }

    public function detail($invoice_no,Order $transaction){
        $transaction = $transaction->newQuery()->whereInvoiceNo($invoice_no)->firstOrFail();
        return view("dashboard.payment.detail",compact("transaction"));
    }
}
