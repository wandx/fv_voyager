<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanyCont extends Controller
{
    public function index(){
        $company = auth()->user()->company;
        return view("dashboard.company.index",compact("company"));
    }

    public function store(Request $request,Company $company){
        $validator = app("validator")->make($request->all(),[
            "name" => "required",
            "address" => "required"
        ]);

        if($validator->fails()){
            return back()->withErrors($validator->messages());
        }
        $request->merge(["user_id"=>$request->user()->id]);
        $store = $company->updateOrCreate(["id"=>$request->input("company_id")],$request->except(["logo","company_id","submit"]));

        if($request->hasFile("logo")){
            $store->uploadMedia($request->file("logo"),["width"=>300,"height"=>300]);
        }
        return back()->with(["success"=>"Company Saved."]);
    }
}
