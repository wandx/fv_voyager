<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Country;
use App\Models\Demand;
use App\Models\Offer;
use App\Models\Order;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;

class DemandCont extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        return view("dashboard.mydemand.index");
    }

    /**
     * @param Country $country
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Country $country){
        $country = $country->newQuery()->pluck("name","id");
        return view("dashboard.mydemand.create",compact("country"));
    }

    /**
     * @param $id
     * @param Demand $demand
     * @param Country $country
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id,Demand $demand,Country $country){
        $demand = $demand->newQuery()->where("user_id",auth()->user()->id)->where("id",$id)->firstOrFail();
        $country = $country->newQuery()->pluck("name","id");
        return view("dashboard.mydemand.edit",compact("demand","country"));
    }

    /**
     * @param Request $request
     * @param Demand $demand
     * @param Order $order
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request,Demand $demand,Order $order){
        $validator = app("validator")->make($request->all(),[
            "title" => "required",
            "min_price" => "required|numeric",
            "max_price" => "required|numeric|min:".$request->input("min_price"),
            "country_id" => "required",
            "description" => "required",
            "doc" => "file|mimes:pdf"
        ]);

        if($validator->fails()){
            return back()->withErrors($validator->messages())->withInput()->with(["active"=>true]);
        }

        if($request->input("submit") == "draft"){
            $request->merge(["status"=>"draft"]);
        }else{
            if(setting("site.base_price") == 0){
                $request->merge(["status"=>"published"]);
            }else{
                $request->merge(["status"=>"pending"]);
            }

        }

        if($request->hasFile("doc_file")){
            $name = "attachment_".Carbon::now()->format("dmYHis").".pdf";
            $request->file("doc_file")->storeAs("public/attachment",$name);
            $request->merge(["doc"=>"public/attachment/{$name}"]);
        }

        $request->merge(["user_id"=>auth()->user()->id]);

        DB::beginTransaction();

        try{
            $d = $demand->newQuery()->create($request->except("submit","_token","doc_file"));
            DB::commit();
            if(setting("site.base_price") == 0){
                return redirect()->route("mydemand.index")->with(["success"=>"Demand Published"]);
            }

            $o = Order::create_order("demand",$d->id);
            return redirect()->route("mypayment.detail",["invoice_no"=>$o->invoice_no])->with(["success"=>"Need payment to publish your demand."]);;
        }catch (\Exception $e){
            try{
                DB::rollBack();
            }catch (\Exception $exception){
                return back()->withErrors(["failed"=>$exception->getMessage()]);
            }
        }


        return redirect()->route("mydemand.index")->with(["success"=>"Demand saved."]);
    }

    /**
     * @param $id
     * @param Request $request
     * @param Demand $demand
     * @param Order $order
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update($id,Request $request,Demand $demand,Order $order){
        $d = $demand->newQuery()->findOrFail($id);
        $validator = app("validator")->make($request->all(),[
            "title" => "required",
            "min_price" => "required|numeric",
            "max_price" => "required|numeric|min:".$request->input("min_price"),
            "country_id" => "required",
            "description" => "required",
            "doc" => "file|mimes:pdf"
        ]);

        if($validator->fails()){
            return back()->withErrors($validator->messages())->withInput()->with(["active"=>true]);
        }

        if($request->input("submit") == "draft"){
            $request->merge(["status"=>"draft"]);
        }else{
            $request->merge(["status"=>"pending"]);
        }

        if($request->hasFile("doc_file")){
            Storage::delete($d->doc);
            $name = "attachment_".Carbon::now()->format("dmYHis").".pdf";
            $request->file("doc_file")->storeAs("public/attachment",$name);
            $request->merge(["doc"=>"public/attachment/{$name}"]);
        }

        $request->merge(["user_id"=>auth()->user()->id]);

        DB::beginTransaction();

        try{
            $d->update($request->except("submit","_token","doc_file"));

            $d = $demand->newQuery()->findOrFail($id);
            if($d->status == "pending"){
                $order->newQuery()->create([
                    "invoice_no" => "DEM".rand(1000000,9999999),
                    "payment_for" => "demand",
                    "amount" => setting("site.base_price",10),
                    "description" => "Payment for updating demand ".$d->title,
                    "status" => "pending",
                    "related_id" => $d->id
                ]);
            }

            DB::commit();

            if($d->status == "pending"){
                // TODO: Redirect to payment process
                return redirect()->route("mydemand.index")->with(["success"=>"Need payment."]);
            }
        }catch (\Exception $e){
            try{
                DB::rollBack();
            }catch (\Exception $exception){}
        }


        return redirect()->route("mydemand.index")->with(["success"=>"Demand saved."]);
    }

    /**
     * @param $id
     * @param Demand $demand
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove_attachment($id,Demand $demand){
        $demand = $demand->newQuery()->findOrFail($id);
        Storage::delete($demand->doc);
        return back()->with(["success"=>"attachment deleted"]);
    }

    /**
     * @param $id
     * @param Demand $demand
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id,Demand $demand){
        // TODO: Prevent demand with offer to be deleted
        $d = $demand->newQuery()->findOrFail($id);
        $d->delete();
        return back()->with(["success"=>"Deleted."]);
    }

    public function offer_list($id,Demand $demand){
        $demand = $demand->newQuery()->where("user_id",auth()->user()->id)->where("id",$id)->firstOrFail();

        $offers = $demand->offers()->latest()->orderBy("is_hired","desc")->paginate(10);

        return view("dashboard.mydemand.offer_lists",compact("offers","demand"));
    }

    public function hire($demand_id,$offer_id,Offer $offer){
        // set flag

        // notify to demand owner

        // notify to offer owner
    }

    public function give_offer($demand_id,Offer $offer,Request $request){
        if($demand_id == auth()->user()->id){
            abort(404);
        }
        $validator = app("validator")->make($request->all(),[
           "title" => "required",
           "description" => "required",
           "price" => "required"
        ]);

        if($validator->fails()){
            return back()->withErrors($validator->messages());
        }

        if($request->hasFile("doc_file")){
            $name = "offer_".Carbon::now()->format("dmYHis").".pdf";
            $request->file("doc_file")->storeAs("public/attachment/",$name);
            $request->merge(["doc"=>"public/attachment/".$name]);
        }

        $request->merge(["demand_id"=>$demand_id]);
        $request->merge(["status"=>setting("site.base_price") == 0 ? "published":"pending"]);
        $request->merge(["user_id"=>auth()->user()->id]);

        $store = $offer->create($request->except(["_token","doc_file"]));

        // redirect to payment
        if(setting("site.base_price") != 0){
            $o = Order::create_order("offer",$store->id);
            return redirect()->route("mypayment.detail",["invoice_no"=>$o->invoice_no]);
        }

        return redirect("dashboard/my-offer")->with(["success"=>"Offer created"]);

    }

}
