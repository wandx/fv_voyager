<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Config;
use Illuminate\Http\Request;
use Log;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Rest\ApiContext;
use Session;
use URL;

class PaypalCont extends Controller
{
    private $_api_context;

    public function __construct()
    {
        /** PayPal api context **/
        $paypal_conf = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
                $paypal_conf['client_id'],
                $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function pay_with_paypal($order_id,Order $order){
        $order = $order->newQuery()->findOrFail($order_id);

        // Payer
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        // Item
        $item = new Item();
        $item->setName($order->description." Invoice number ".$order->invoice_no)
             ->setCurrency("USD")
             ->setQuantity(1)
             ->setPrice($order->amount);

        $item_list = new ItemList();
        $item_list->setItems([$item]);

        // Amount
        $amount = new Amount();
        $amount->setCurrency("USD")
               ->setTotal($order->amount);

        // Transaction
        $transaction = new Transaction();
        $transaction->setAmount($amount)
                    ->setDescription($order->description." Invoice number ".$order->invoice_no);

        // Redirect url
        $redirect_url = new RedirectUrls();
        $redirect_url->setReturnUrl(route("get_payment_status",["invoice_no"=>$order->invoice_no])) // TODO: set return status url
                     ->setCancelUrl(route("get_payment_status",["invoice_no"=>$order->invoice_no]));// TODO: set cancel status url

        $payment = new Payment();
        $payment->setIntent("sale")
                ->setPayer($payer)
                ->setRedirectUrls($redirect_url)
                ->setTransactions([$transaction]);

        try{
            $payment->create($this->_api_context);
        }catch (PayPalConnectionException $ex){
            if (\Config::get('app.debug')) {
                Log::error($ex->getMessage());
                return redirect()->route('mypayment.detail',["invoice_no"=>$order->invoice_no])->withErrors(["failed"=>$ex->getMessage()]); //TODO: redirect to order detail
            } else {
                Log::error("Some error occur, sorry for inconvenient");
                return redirect()->route('mypayment.detail',["invoice_no"=>$order->invoice_no])->withErrors(["failed"=>$ex->getMessage()]);
            }
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return redirect()->away($redirect_url);
        }
//        Session::put('error', 'Unknown error occurred');
        return redirect()->route('mypayment.detail',["invoice_no"=>$order->invoice_no])->withErrors(["failed"=>"Unknown error occurred"]);
    }

    public function get_payment_status(Request $request,$invoice_no){
        $payment_id = session("paypal_payment_id");

        session()->forget("paypal_payment_id");

        if(!$request->has("PayerID") || !$request->has("token")){
            return redirect()->route("mypayment.detail",["invoice_no"=>$invoice_no])->withErrors(["Payment Failed"]);
        }

        $payment = Payment::get($payment_id,$this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId($request->input("PayerID"));

        $result = $payment->execute($execution,$this->_api_context);

        if($result->getState() == "approved"){
            (new Order())->newQuery()->where("invoice_no",$invoice_no)->update(["status"=>"paid"]);
            (new Order())->newQuery()->where("invoice_no",$invoice_no)->update(["message"=>"Approved"]);
            (new Order())->newQuery()->where("invoice_no",$invoice_no)->first()->related()->update(["status"=>"published"]);
            return redirect()->route("mypayment.detail",["invoice_no"=>$invoice_no])->with(["success"=>"Payment Success."]);
        }
        (new Order())->newQuery()->where("invoice_no",$invoice_no)->update(["message"=>"Failed"]);
        return redirect()->route("mypayment.detail",["invoice_no"=>$invoice_no])->withErrors(["failed"=>"Payment Failed."]);
    }

    public function payWithpaypal(Request $request)
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $item_1 = new Item();
        $item_1->setName('Item 1') /** item name **/
        ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($request->get('amount')); /** unit price **/
        $item_list = new ItemList();
        $item_list->setItems(array($item_1));
        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal($request->get('amount'));
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your transaction description');
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('status')) /** Specify return URL **/
        ->setCancelUrl(URL::route('status'));
        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; **/
        try {
            $payment->create($this->_api_context);
        } catch (PayPalConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('error', 'Connection timeout');
                return Redirect::route('paywithpaypal');
            } else {
                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::route('paywithpaypal');
            }
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }
        \Session::put('error', 'Unknown error occurred');
        return Redirect::route('paywithpaypal');
    }

}
