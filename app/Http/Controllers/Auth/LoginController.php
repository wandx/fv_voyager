<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard/userinfo';

    public function showLoginForm()
    {
        return view("login");
    }

    protected function credentials(Request $request)
    {
        return [
            $this->username() => $request->input($this->username()),
            "password"=>$request->input("password"),
//            "is_suspend"=>0,
//            "is_active" =>1
        ];
    }
}
