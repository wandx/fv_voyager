<?php

namespace App\Listeners;

use App\Events\DemandCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddInvoice
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DemandCreated  $event
     * @return void
     */
    public function handle(DemandCreated $event)
    {
        //
    }
}
