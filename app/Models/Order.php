<?php

namespace App\Models;

use App\Traits\UuidForKey;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class Order extends Model
{
    use UuidForKey;

    protected $guarded = ["id"];
    public $incrementing = false;

    public static function create_order($for,$related_id,$status="pending"){
        $store = Order::create([
            "invoice_no" => self::build_invoice($for == "offer" ? "OFR":"DEM"),
            "payment_for" => $for,
            "related_id" => $related_id,
            "amount" => setting("site.base_price"),
            "description" => $for == "offer" ? "Pay for create offer":"Pay for create demand.",
            "status" => "pending",
            "user_id" => auth()->user()->id
        ]);

        return $store;
    }

    public function related(){
        if($this->payment_for == "offer"){
            return $this->belongsTo(Offer::class,"related_id");
        }
        return $this->belongsTo(Demand::class,"related_id");
    }

    private static  function build_invoice($prefix){
        $num = Carbon::now()->format("ymdHis");
        return strtoupper($prefix).$num;
    }
}
