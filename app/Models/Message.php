<?php

namespace App\Models;

use App\Traits\UuidForKey;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use UuidForKey;

    public $incrementing = false;
    protected $guarded = ["id"];
    protected $appends = ["unread_message"];

    public function sender(){
        return $this->belongsTo(User::class,"sender_id");
    }

    public function receiver(){
        return $this->belongsTo(User::class,"receiver_id");
    }

    public function message_items(){
        return $this->hasMany(MessageItem::class);
    }

    public function getUnreadMessageAttribute(){
        $user = auth()->user()->id;
        $id = $this->id;

        return $this->message_items()->where("user_id","!=",$user)->where("is_read",0)->count() ?? 0;
    }
}
