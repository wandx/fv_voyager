<?php

namespace App\Models;

use App\Traits\UuidForKey;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    use UuidForKey;

    protected $guarded = ["id"];
    public $incrementing = false;

    public function demand(){
        return $this->belongsTo(Demand::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
