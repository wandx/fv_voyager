<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $guarded = ["id"];

    public function demands(){
        return $this->hasMany(Demand::class);
    }
}
