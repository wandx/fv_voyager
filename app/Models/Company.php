<?php

namespace App\Models;

use App\Traits\UuidForKey;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use Storage;

class Company extends Model
{
    use UuidForKey;

    protected $guarded = ["id"];
    public $incrementing = false;

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function uploadMedia($content,array $config = []){
        $width = $config["width"] ?? 0;
        $height = $config["height"] ?? 0;
        $company_id = $this->id ?? null;

        $im = Image::make($content);

        if($width != 0 && $height != 0){
            $im->fit($width,$height);
        }

        $im->encode("jpg");

        if($company_id != null){
            $path = $this->find($company_id)->company_logo;
            if(Storage::exists($path)){
                Storage::delete($path);
            }
        }

        $path = "public/images/";
        $name = str_random(10).".jpg";
        $filename = $path.$name;

        Storage::put($filename,$im->__toString());

        if($company_id != null){
            $this->find($company_id)->update(["company_logo"=>$filename]);
        }

        return $filename;
    }
}
