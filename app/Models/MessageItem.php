<?php

namespace App\Models;

use App\Traits\UuidForKey;
use App\User;
use Illuminate\Database\Eloquent\Model;

class MessageItem extends Model
{
    use UuidForKey;

    protected $guarded = ["id"];
    public $incrementing = false;

    public function message(){
        return $this->belongsTo(Message::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
