<?php

namespace App\Models;

use App\Traits\UuidForKey;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use UuidForKey;
    protected $guarded = ["id"];
    public $incrementing = false;
}
