<?php
function convertYoutube($string) {
    return preg_replace(
        "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
        "<iframe style=\"z-index: 1; position: absolute;width: 100%;height: 100%;object-fit: fill;\" src=\"//www.youtube.com/embed/$2\" allowfullscreen></iframe>",
        $string
    );
}