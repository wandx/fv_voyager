<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::post('login', ['uses' => "Admin\AuthController@postLogin", 'as' => 'postlogin']);
});

Route::get("login","Auth\LoginController@showLoginForm")->name("login");
Route::post("login","Auth\LoginController@login");

Route::group(["prefix"=>"demand","namespace"=>"Site"],function(){
    Route::get("/","DemandCont@index")->name("demand.index");
    Route::get("{id}/single","DemandCont@single")->name("demand.single");
//    Route::post("{id}/single","DemandCont@submit_offer")->name("demand.submit_offer");
});


Route::group(["middleware"=>"auth"],function(){
    Route::get("logout","Auth\LoginController@logout");
    Route::group(["namespace"=>"Dashboard","prefix"=>"dashboard"],function(){
        Route::group(["prefix"=>"userinfo","as"=>"userinfo."],function(){
            Route::get("/","UserInfoCont@index")->name("index");
            Route::post("/","UserInfoCont@update")->name("update");
        });

        Route::group(["prefix"=>"my-demand","as"=>"mydemand."],function(){
            Route::get("/","DemandCont@index")->name("index");
            Route::get("create","DemandCont@add")->name("create");
            Route::post("create","DemandCont@store")->name("store");
            Route::get("{id}/edit","DemandCont@edit")->name("edit");
            Route::post("{id}/edit","DemandCont@update")->name("update");
            Route::get("{id}/delete-attachment","DemandCont@remove_attachment")->name("delete_atachment");
            Route::get("{id}/delete","DemandCont@destroy")->name("delete");
            Route::get("offer-lists/{demand_id}","DemandCont@offer_list")->name("offer_lists");
            Route::post("give-offer/{demand_id}","DemandCont@give_offer")->name("give_offer");
        });

        Route::group(["prefix"=>"my-offer","as"=>"myoffer."],function(){
            Route::get("/","OfferCont@index")->name("index");
        });

        Route::group(["prefix"=>"my-company","as"=>"mycompany."],function(){
            Route::get("/","CompanyCont@index")->name("index");
            Route::post("/","CompanyCont@store")->name("store");
        });

        Route::group(["prefix"=>"my-message","as"=>"mymessage."],function(){
            Route::get("/","MessageCont@index")->name("index");
            Route::get("{message_id}/show","MessageCont@show")->name("show");
            Route::post("{message_id}/reply","MessageCont@reply")->name("reply");
            Route::post("send_message","MessageCont@send_message")->name("send_message");
        });

        Route::group(["prefix"=>"my-payment","as"=>"mypayment."],function(){
           Route::get("/","PaymentCont@index")->name("index");
           Route::get("{invoice_no}/detail","PaymentCont@detail")->name("detail");
        });
   });

    //pp
    Route::get("my-payment/{order_id}/status","PaypalCont@get_payment_status")->name("get_payment_status");
    Route::get("my-payment/{order_id}/pay-paypal","PaypalCont@pay_with_paypal")->name("pay_paypal");
});
