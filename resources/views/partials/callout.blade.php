<!-- start section callout -->
<div class="jbm-section-callout">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 callout-bg-1 callout-section-left pos-relative">
                <div class="callout-bg">
                    {{--<video style="z-index: 1; position: absolute;width: 100%;height: 100%;object-fit: fill;" src="{{ asset("videos/videoplayback.mp4") }}" loop muted autoplay></video>--}}
                    {!! convertYoutube(setting('video.left')) !!}
                </div>
                <div class="jbm-callout-in jbm-callout-in-left pull-right text-left">
                    <div class="jbm-section-title margin-top-100 margin-bottom-100 title-white" style="height: 300px"></div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 callout-bg-1 callout-section-right pos-relative">
                <div class="callout-bg">
                    {{--<video style="z-index: 1; position: absolute;width: 100%;height: 100%;object-fit: fill;" src="{{ asset("videos/videoplayback.mp4") }}" loop muted autoplay></video>--}}
                    {!! convertYoutube(setting('video.right')) !!}
                </div>
                <div class="jbm-callout-in jbm-callout-in-left pull-right text-left">
                    <div class="jbm-section-title margin-top-100 margin-bottom-100 title-white" style="height: 300px"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end section callout -->