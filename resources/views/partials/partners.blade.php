<!-- start section helpbox -->
<div class="jbm-section-partners padding-bottom-85">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <ul class="list-none dis-inline jbm-partner-list">
                    <li class="jbm-partner-box">
                        <a href="#">
                            <img src="{{ asset('assets/img/photodune.png') }}" alt="photodune" />
                        </a>
                    </li>
                    <li class="jbm-partner-box">
                        <a href="#">
                            <img src="{{ asset('assets/img/graphicriver.png') }}" alt="graphicriver" />
                        </a>
                    </li>
                    <li class="jbm-partner-box">
                        <a href="#">
                            <img src="{{ asset('assets/img/codecanyon.png') }}" alt="codecanyon" />
                        </a>
                    </li>
                    <li class="jbm-partner-box">
                        <a href="#">
                            <img src="{{ asset('assets/img/audiojungle.png') }}" alt="audiojungle" />
                        </a>
                    </li>
                    <li class="jbm-partner-box">
                        <a href="#">
                            <img src="{{ asset('assets/img/themeforest.png') }}" alt="themeforest" />
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>