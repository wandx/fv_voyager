<!-- start section title -->
<div class="jbm-section-title">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <span class="section-tit-line"></span>
                <h2>{{ setting('warranty.title',"Warranty") }}</h2>
                <p>{{ setting('warranty.subtitle','Subtitle') }}</p>
            </div>
        </div>
    </div>
</div>
<!-- end section title -->