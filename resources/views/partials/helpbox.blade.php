<!-- start section helpbox -->
<div class="jbm-section-helpbox main-1st-bg padding-top-75 padding-bottom-100">
    <!-- start section title -->
    <div class="jbm-section-title title-white">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <span class="section-tit-line"></span>
                    <h2 class="white">{{ setting('help-section.title',"Need Help ?") }}</h2>
                    <p>{{ setting('help-section.subtitle','Subtitle') }}</p>
                </div>
            </div>
        </div>
    </div>
    <!-- end section title -->
</div>
<!-- end section helpbox -->