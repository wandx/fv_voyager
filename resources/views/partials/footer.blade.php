<footer>
    <div class="footer-bottom-bar main-2nd-bg padding-top-115 margin-top-30" style="margin-bottom: -20px;">
        <div class="container">
            <div class="row">
                <div class="col-md-5 hidden-xs hidden-sm text-left">
                    <p>Made with <i class="fa fa-heart"></i> by PSD_Market. All Rights Reserved.</p>
                </div>
                <div class="col-md-2 text-center back-top">
                    <a href="#" class="back-top-button">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a href="#" class="back-top-link">Back to Top</a>
                </div>
                <div class="col-md-5 text-right">
                    <ul class="list-none jbm-social-icon-1 ">
                        <li>
                            <a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-behance"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-5 hidden-md hidden-lg text-center">
                    <p>Made with <i class="fa fa-heart"></i> by PSD_Market. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Spinner -->
<div class="jbm-spinner">
    <div class="spinner-content">
        <div class="loader-circle"></div>
        <div class="loader-line-mask">
            <div class="loader-line"></div>
        </div>
    </div>
</div>
<!-- Spinner end here -->`
