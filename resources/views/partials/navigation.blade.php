<!-- start menu -->
<div class="jbm-mennubar">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                <div class="jbm-logo">
                    <a href="/">
                        <img src="{{ Voyager::image(setting("site.logo"),asset("img/logo2.png")) }}" alt="{{ setting("site.title") }}" class="img-responsive" />
                    </a>
                </div>
                <div class="jbm-menu-icon pull-right hidden-lg">
                    <a href="#">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 302 302" xml:space="preserve" ><g><rect y="36" width="302" height="30" fill="#454545"/><rect y="236" width="302" height="30" fill="#454545"/><rect y="136" width="302" height="30" fill="#454545"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
                    </a>
                </div>
            </div>
            @include("partials.nav_menu")
        </div>
    </div>
</div>