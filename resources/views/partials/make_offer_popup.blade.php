<!-- Spinner end here -->
<div class="apply-job-popup">
    <div class="popup-overlay"></div>
    <!-- Candidate popup -->
    <div class="popup">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-md-5 jbm-login-side applyy">
                        <i class="fa fa-lightbulb-o margin-bottom-50" aria-hidden="true"></i>
                        <span class="section-tit-line"></span>
                        <h3 class="margin-bottom-60">Make your best offer</h3>
                        <ul class="new-signup">
                            @if(!auth()->check())
                            <li>
                                <a href="{{ url("/register") }}">New User? </a>
                            </li>
                            <li>
                                <a href="{{ url("/register") }}">Sign Up</a>
                            </li>
                            @else
                                <li>&nbsp;</li>
                            @endif
                        </ul>

                    </div>
                    <div class="col-md-7 jbm-form">
                        <form enctype="multipart/form-data" action="{{ route("mydemand.give_offer",["demand_id"=>$demand->id]) }}" class="jbm-field margin-top-20 register" method="post">
                            {!! csrf_field() !!}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="title" id="title" class="form-control">
                                    <label for="title">Title*</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="number" name="price" id="price" class="form-control">
                                    <label for="price">Price to offer*</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea name="description" id="description" class="form-control"></textarea>
                                    <label for="description">Describe what you offer</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group field-active">
                                    <input style="height: auto;" type="file" name="doc_file" id="attachment" class="form-control">
                                    <label for="attachment">Document(PDF)</label>
                                </div>
                            </div>
                            {{--<div class="g-recaptcha" data-sitekey="6LdcpCoUAAAAAH7ei-XX2bHUWul6Ppl5wYm1Y7Ne"></div>--}}
                            <button type="submit" class="jbm-button jbm-button-3 jbm-hover margin-bottom-15">Next: Payment</button>
                            <div class="terms">
                                <input type="checkbox" id="c1" name="cc">
                                <label for="c1"><span></span></label>
                                <small>I have read and agree with <a href="#">Terms & Conditions</a></small>
                            </div>
                        </form>
                    </div>
                    <div class="close-btn">
                        <i class="fa fa-close"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Candidate popup -->
</div><!--apply job popup end-->