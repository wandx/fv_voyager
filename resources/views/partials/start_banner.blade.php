<!-- start banner -->
<div class="banner">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="jbm-banner-text text-center">
                    <div class="jbm-ban-txt-line-1">
                        <h1>
                            {{ setting('big-image.bold_text',"Bold Text") }} <span class="double-line">{{ setting('big-image.bold_text',"Line Text") }}</span>
                        </h1>
                    </div>
                    <div class="jbm-ban-txt-line-2">
                        <p class="blockquote">“{{ setting("big-image.quote","This is quote") }}”</p>
                    </div>
                    <div class="jbm-ban-txt-line-3">
                        <a href="/demand" class="jbm-button jbm-button-5 margin-right-30">Search Vendors</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end banner -->

@push("on_header")
    <style>
        .banner{
            background-image: url("{{ Voyager::image(setting("big-image.image"),asset("img/Slider_01.jpg")) }}");
        }
    </style>
@endpush