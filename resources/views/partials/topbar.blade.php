<!-- start topbar -->
<div class="jbm-topbar">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 text-left">
                <ul class="dis-inline list-none jbm-topbar-contact">
                    <li>
                        <a href="tel:+0001234567890"><i class="fa fa-phone"></i>+ 000 123 456 7890</a>
                    </li>
                    <li>
                        <a href="mailto:contactadmin@jobmarket.com"><i class="fa fa-envelope"></i>contactadmin@jobmarket.com</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 text-right hidden-xs hidden-sm">
                <ul class="dis-inline list-none  jbm-topbar-login">
                    @if(auth()->check())
                        <li>
                            <a href="/dashboard/userinfo">Dashboard</a>
                        </li>
                    @else
                        <li>
                            <a href="/login">Login</a>
                        </li>
                        <li>
                            <a href="/register">Register</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- end topbar -->