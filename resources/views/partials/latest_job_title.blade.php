<!-- start section title -->
<div class="jbm-section-title margin-top-100">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <span class="section-tit-line"></span>
                <h2>{{ setting('latest-job-section.title',"Latest Job") }}</h2>
                <p>{{ setting('latest-job-section.subtitle',"Subtitle") }}</p>
            </div>
        </div>
    </div>
</div>
<!-- end section title -->