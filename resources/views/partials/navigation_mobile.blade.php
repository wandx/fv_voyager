<!-- start mobile menu -->
<div class="jbm-menu-wrap">
    <div class="jbm-mobile-logo-wrap">
        <div class="jbm-logo">
            <a href="/">
                <img src="{{ Voyager::image(setting("site.logo"),asset("img/logo2.png")) }}" alt="{{ setting("site.title") }}" class="img-responsive" />
            </a>
        </div>
        <div class="jbm-menu-icon pull-right">
            <a href="#">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 612 612" xml:space="preserve"><g><g id="cross"><g><polygon points="612,36.004 576.521,0.603 306,270.608 35.478,0.603 0,36.004 270.522,306.011 0,575.997 35.478,611.397 306,341.411 576.521,611.397 612,575.997 341.459,306.011 " fill="#454545"/></g></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
            </a>
        </div>
    </div>
    {{--<div class="jbm-mobile-search-wrap margin-bottom-50">--}}
        {{--<form class="form-inline" method="post" >--}}
            {{--<div class="form-group m-r-4">--}}
                {{--<input type="text" class="form-control" name="s" id="header-search-input2" placeholder="Search here..." />--}}
            {{--</div>--}}
            {{--<div class="form-group pull-right">--}}
                {{--<button type="submit" class="jbm-search-sm-btn">--}}
                    {{--<i class="fa fa-search"></i>--}}
                {{--</button>--}}
            {{--</div>--}}
        {{--</form>--}}
    {{--</div>--}}
    @include("partials.nav_menu_mobile")
</div>