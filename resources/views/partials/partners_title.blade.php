<!-- start section title -->
<div class="jbm-section-title margin-top-100 margin-bottom-80">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <span class="section-tit-line"></span>
                <h2>{{ setting('partner-section.title',"Our best partner") }}</h2>
                <p>{{ setting('partner-section.subtitle','Subtitle') }}</p>
            </div>
        </div>
    </div>
</div>
<!-- end section title -->