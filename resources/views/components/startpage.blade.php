<div class="jbm-page-title page-title-bg-2 {{ $additional_attribute ?? 'margin-bottom-80' }}">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <span class="section-tit-line"></span>
                <h2>
                    {{ $title ?? "Dashboard" }}
                </h2>
                <p>
                <a href="/"><i class="fa fa-home" aria-hidden="true"></i></a>
                @if(isset($breadcrumb) && is_array($breadcrumb))
                    &nbsp; > &nbsp;
                    @foreach($breadcrumb as $k=>$v)
                        @if(!$loop->last)
                            <a href="{{ $k }}">{{ $v }}</a> &nbsp; > &nbsp;
                        @else
                            {{ $v }}
                        @endif
                    @endforeach
                @endif
                </p>
            </div>
        </div>
    </div>
</div>