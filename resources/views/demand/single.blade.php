@extends("master")

@push("on_body")
    @component("components.startpage",["additional_attribute"=>"","title"=>$demand->title,"breadcrumb"=>[url("demands")=>"Demands","#"=>$demand->title]])
    @endcomponent

    <br>
    <br>
    <div class="job-info padding-top-60">
        <div class="container">
            @include("partials.error")
            @include("partials.success")

            @if(auth()->check() && auth()->user()->id == $demand->user_id)
            <div class="alert alert-warning">
                You are owner of this demand. <a href="{{ route("mydemand.edit",["id"=>$demand->id]) }}">Edit demand</a>
            </div>
            @endif
            <div class="row margin-bottom-60">
                <div class="col-md-7 col-sm-6 col-xs-6 full-wdth">
                    <!-- Left-aligned -->
                    <div class="media">
                        <div class="media-left">
                            <img src="{{ $demand->logo }}" alt="{{ $demand->title }}" class="media-object" style="width: 70px;"/>
                        </div>
                        <div class="media-body style2">
                            <h5 class="media-heading style2">
                                {{ $demand->user->company->name ?? $demand->user->name }}
                                @if(auth()->check() && auth()->user()->id != $demand->user_id)
                                    <a href="#" onclick="event.preventDefault()" data-toggle="modal" data-target="#myModal" style="display: block;"><small>Send Message</small></a>
                                @endif
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-6 full-wdth">
                    <div class="candidate-bottom padding-top-30 text-right stl2">
                        @if(auth()->check() && auth()->user()->id != $demand->user_id && $demand->is_hired == 0)

                        <a href="#" class="jbm-button apply-btn jbm-button-3">Give Offer</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <!-- candidate personal info -->
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="candidate-personal-info jbm-relative jbm-accordion">
                        <div class="row">
                            <div class="col-md-3 margin-bottom-30">
                                <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;{{ $demand->user->company->address ?? "Unknown" }}</a>
                            </div>
                            <div class="col-md-3 margin-bottom-30">
                                <a target="_blank" href="{{ $demand->user->company->website ?? "#" }}"><i class="fa fa-globe" aria-hidden="true"></i>&nbsp;{{ $demand->user->company->website ?? "No Website" }}</a>
                            </div>
                            <div class="col-md-3 margin-bottom-30">
                                <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;{{ $demand->user->email }}</a>
                            </div>
                            <div class="col-md-3 margin-bottom-30">
                                <a href="#"><i class="fa fa-money" aria-hidden="true"></i>&nbsp;{{ $demand->price_range }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if($demand->user->company != null && $demand->user->company->description != "")
        <!-- Brief Description -->
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="jbm-job-content">
                        <h5>Company Profile</h5>
                        <p>
                            {{ $demand->user->company->description }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        @endif

    <!-- Brief Description -->
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="jbm-job-content">
                        <h5>Demand Description</h5>
                        <p>
                            {{ $demand->description }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endpush

@push("modals")
    @include("partials.make_offer_popup")

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            {!! Form::open(["route"=>"mymessage.send_message","class"=>"modal-content"]) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Send {{ $demand->user->company->name ?? $demand->user->name }} message</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="receiver_id" value="{{ $demand->user->id }}">
                    <div class="form-group">
                        <textarea name="body" required id="body" class="form-control" rows="4"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Send</button>
                </div>
            {!! Form::close() !!}

        </div>
    </div>
@endpush