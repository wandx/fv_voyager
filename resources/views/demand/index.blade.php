@extends("master")

@push("on_body")
    @component("components.startpage",["additional_attribute"=>"","title"=>"Demand Lists"])
    @endcomponent
    @widget("HomeSearch")
    <br>
    <br>
    <div class="container">

        <h5>
            @if($request->has("keywords"))
                Keywords: {{ $request->input("keywords") }}
            @endif
            &nbsp;
            &nbsp;
            @if($request->has("country"))
                Country: {{ $request->input("country") }}
            @endif
        </h5>
        @forelse($demands as $demand)
            @widget("DemandItem",[
                "logo" => $demand->logo,
                "company_name" => $demand->user->company->name ?? $demand->user->name,
                "city_name" => $demand->country->name ?? "Unknown",
                "title" => $demand->title,
                "min_price" => $demand->min_price,
                "max_price" => $demand->max_price,
                "id" => $demand->id,
            ])
        @empty
            <div class="well margin-bottom-50">
                <div class="text-center">No data</div>
            </div>
        @endforelse
        <div class="text-center">
            {!! $demands->render() !!}
        </div>
    </div>

@endpush

