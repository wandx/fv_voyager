@if($partners->count() > 0)
    @include("partials.partners_title")

    <div class="jbm-section-partners padding-bottom-85">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <ul class="list-none dis-inline jbm-partner-list">
                        @foreach($partners as $partner)
                            <li class="jbm-partner-box">
                                <a href="{{ $partner->url }}">
                                    <img src="{{ Voyager::image($partner->image) }}" alt="{{ $partner->name }}" />
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endif