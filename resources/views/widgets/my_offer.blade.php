<div class="job-posted padding-bottom-20 table-sm-setting">
    <div class="row margin-bottom-20">
        <div class="col-md-3 col-sm-4 col-xs-2">
            <strong class="margin-bottom-20">Demand</strong>
            <span class="section-tit-line-2 margin-bottom-40"></span>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-2">
            <strong class="margin-bottom-20">Budget</strong>
            <span class="section-tit-line-2 margin-bottom-40"></span>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-2">
            <strong class="margin-bottom-20">Offer Price</strong>
            <span class="section-tit-line-2 margin-bottom-40"></span>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-2">
            <strong class="margin-bottom-20">Offer Date</strong>
            <span class="section-tit-line-2 margin-bottom-40"></span>
        </div>
        <div class="col-md-1 col-sm-1 col-xs-2">
            <strong class="margin-bottom-20">Status</strong>
            <span class="section-tit-line-2 margin-bottom-40"></span>
        </div>
        <div class="col-md-1 col-sm-1 col-xs-2">
        </div>
    </div>
    @forelse($offers as $offer)
        <div class="row margin-bottom-40">
            <div class="col-md-3 col-sm-4 col-xs-2">
                <span class="margin-bottom-20">
                    <a href="{{ route("demand.single",["id"=>$offer->demand->id]) }}">
                        {{ $offer->demand->title }}
                    </a>
                    &nbsp;
                    <a href="#" class="btn btn-xs {{ $offer->count() > 0 ? "btn-info":"btn-default" }}" style="padding: 1px 8px;"><i class="fa fa-envelope"></i></a>

                </span>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-2">
                <span class="margin-bottom-20">${{ $offer->demand->min_price }} - ${{ $offer->demand->max_price }}</span>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-2">
                <span class="margin-bottom-20">${{ $offer->price }}</span>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-2">
                <span class="margin-bottom-20">{{ $offer->created_at->format("d M Y") }}</span>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-2">
                <span class="margin-bottom-20">{{ $offer->status }}</span>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-2"></div>
        </div>
        @empty
            <div class="text-center">
                No Offer
            </div>
    @endforelse
    <div class="text-center">
        {!! $offers->render() !!}
    </div>

</div><!--job-posted end-->
