<!-- start search -->
<div class="jbm-search-bar jbm-search-1">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="jbm-sch-inner margin-top-85-minus">
                    <form class="row" method="get">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input value="{{ Request::get("keywords") }}" type="text" class="form-control" id="keyword" name="keywords" placeholder="Search Your Keyword" />
                                {{--<p>(Example: web design, seo analyst)</p>--}}
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                {!! Form::select("country",$country,Request::get("country"),["class"=>"form-control","id"=>"location","placeholder"=>"Select Country"]) !!}
                                {{--<input type="text" class="form-control" id="location" name="country" placeholder="Location" />--}}
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group ">
                                <input type="submit" class="jbm-button jbm-button-3" id="search-btn" />
                                {{--<p class="text-center"><a href="#">More Search Options</a></p>--}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end search -->