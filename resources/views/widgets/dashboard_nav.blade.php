<!-- Emp sidebar -->
<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="jbm-emp-sidebar padding-bottom-30 padding-top-30">
        <ul class="jbm-dashboard-links">
            @foreach($config["list"] as $list)
                <li>
                    <a href="{{ $list["url"] }}" class="{{ $list["slug"]== $config["active"] ? 'active':'' }}">{{ $list["text"]}}</a>
                </li>
            @endforeach
        </ul>
    </div>
</div>