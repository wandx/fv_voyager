<div class="job-posted padding-bottom-20 table-sm-setting">
    <div class="row margin-bottom-20">
        <div class="col-xs-3">
            <strong class="margin-bottom-20">Title</strong>
            <span class="section-tit-line-2 margin-bottom-40"></span>
        </div>
        <div class="col-xs-1 text-center">
            <strong class="margin-bottom-20">Country</strong>
            <span class="section-tit-line-2 margin-bottom-40"></span>
        </div>
        <div class="col-xs-2 text-center">
            <strong class="margin-bottom-20">Price (USD)</strong>
            <span class="section-tit-line-2 margin-bottom-40"></span>
        </div>
        <div class="col-xs-3">
            <strong class="margin-bottom-20">Description</strong>
            <span class="section-tit-line-2 margin-bottom-40"></span>
        </div>
        <div class="col-xs-1">
            <strong class="margin-bottom-20 text-center">Offers</strong>
            <span class="section-tit-line-2 margin-bottom-40"></span>
        </div>
        <div class="col-xs-1">
            <strong class="margin-bottom-20">Status</strong>
            <span class="section-tit-line-2 margin-bottom-40"></span>
        </div>
        <div class="col-xs-1">
        </div>
    </div>
    @forelse($demands as $demand)
        <div class="row margin-bottom-40">
        <div class="col-xs-3 demand-title">
            <span class="margin-bottom-20">
                <a href="#">
                    {{ $demand->title }}
                </a>
                <br>
                @if($demand->offers()->count() == 0)
                <small class="act hidden"> <a href="{{ route("mydemand.edit",["id"=>$demand->id]) }}">Edit</a></small>
                @endif
            </span>
        </div>
        <div class="col-xs-1 text-center">
            <span class="margin-bottom-20">{{ $demand->country->code ?? 0 }}</span>
        </div>
        <div class="col-xs-2 text-center">
            <span class="margin-bottom-20">
                {{ $demand->min_price ?? 0 }} - {{ $demand->max_price ?? 0 }}
            </span>
        </div>
        <div class="col-xs-3">
            <span class="margin-bottom-20">{{ $demand->short_description }}</span>
        </div>
        <div class="col-xs-1 text-center">
            <span class="margin-bottom-20"><a href="{{ route("mydemand.offer_lists",["demand_id"=>$demand->id]) }}">{{ $demand->offers()->count() }}</a></span>
        </div>
        <div class="col-xs-1">
            <span class="margin-bottom-20">{{ $demand->status }}</span>
        </div>
        <div class="col-xs-1 text-right">
            <span class="margin-bottom-20">
                @if($demand->offers()->count() == 0)
                <a onclick="return confirm('Delete {{ $demand->title }} ?')" href="{{ route("mydemand.delete",["id"=>$demand->id]) }}" class="fa fa-trash"></a>
                @endif
            </span>
        </div>
        </div>
        @empty
            <div class="text-center">
                No demand.
            </div>
    @endforelse
    <div class="text-center">
        {!! $demands->render() !!}
    </div>
</div>

@push("scripts")
    <script>
        $(document).on("mouseover",".demand-title",function(){
             $(this).find(".act").toggleClass("hidden");
        });

        $(document).on("mouseout",".demand-title",function(){
            $(this).find(".act").toggleClass("hidden");
        });
    </script>
@endpush
