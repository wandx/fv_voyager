<div class="job-posted padding-bottom-20 table-sm-setting">
    <div class="row margin-bottom-20">
        <div class="col-xs-2">
            <strong class="margin-bottom-20">Invoice No.</strong>
            <span class="section-tit-line-2 margin-bottom-40"></span>
        </div>
        <div class="col-xs-1 text-center">
            <strong class="margin-bottom-20">Status</strong>
            <span class="section-tit-line-2 margin-bottom-40"></span>
        </div>
        <div class="col-xs-2 text-center">
            <strong class="margin-bottom-20">Type</strong>
            <span class="section-tit-line-2 margin-bottom-40"></span>
        </div>
        <div class="col-xs-3">
            <strong class="margin-bottom-20">Description</strong>
            <span class="section-tit-line-2 margin-bottom-40"></span>
        </div>
        <div class="col-xs-3">
            <strong class="margin-bottom-20">Message</strong>
            <span class="section-tit-line-2 margin-bottom-40"></span>
        </div>
        {{--<div class="col-xs-1">--}}
        {{--</div>--}}
    </div>
    @forelse($transactions as $transaction)
        <div class="row margin-bottom-40">
            <div class="col-xs-2">
                <span class="margin-bottom-20">
                    <a href="{{ route("mypayment.detail",["invoice_no"=>$transaction->invoice_no]) }}">{{ $transaction->invoice_no }}</a><br>
                </span>
            </div>
            <div class="col-xs-1">
            <span class="margin-bottom-20">
                {{ $transaction->status }}
            </span>
            </div>
            <div class="col-xs-2 text-center">
                <span class="margin-bottom-20">{{ $transaction->payment_for }}</span>
            </div>
            <div class="col-xs-3">
                <span class="margin-bottom-20">{{ $transaction->description }}</span>
            </div>
            <div class="col-xs-3">
                <span class="margin-bottom-20">{{ $transaction->message ?? "-" }}</span>
            </div>
            {{--<div class="col-xs-1 text-right">--}}
            {{--<span class="margin-bottom-20">--}}
                {{--<a onclick="return confirm('Delete {{ $transaction->title }} ?')" href="#" class="fa fa-trash"></a>--}}
            {{--</span>--}}
            {{--</div>--}}
        </div>
    @empty
        <div class="text-center">
            No Payment.
        </div>
    @endforelse
    <div class="text-center">
        {!! $transactions->render() !!}
    </div>
</div>

@push("scripts")
    <script>
        $(document).on("mouseover",".demand-title",function(){
            $(this).find(".act").toggleClass("hidden");
        });

        $(document).on("mouseout",".demand-title",function(){
            $(this).find(".act").toggleClass("hidden");
        });
    </script>
@endpush
