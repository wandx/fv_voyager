<div class="jbm-job-loop-in">
    <div class="row">
        <div class="col-md-3 col-sm-5 col-xs-5 full-wdth mg-btm-20 text-left jbm-first-col">
            <div class="row">
                <div class="col-xs-4">
                    <div class="jbm-company-logo">
                        @if($config["logo"] == null)
                            {{ strtoupper(substr($config["title"],0,1)) }}
                        @else
                            <img src="{{ $config["logo"] }}" alt="{{ strtoupper($config['title']) }}" style="width: 80px;">
                        @endif
                    </div>
                </div>
                <div class="col-xs-8">
                    <div class="jbm-job-title">
                        <a href="#" class="title-link">{{ $config["title"] }}</a>
                        <br />
                        <a href="#" class="jbm-job-email">@ {{ $config["company_name"] }}</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-4 text-center">
            <div class="jbm-job-locaction">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <br />
                <a href="#">{{ $config["city_name"] }}</a>
            </div>
        </div>
        <div class="col-md-4 col-sm-5 col-xs-8 text-center">
            <div class="jbm-job-price">
                <i class="fa fa-money" aria-hidden="true"></i>
                <br />
                <span>{{ $config["min_price"] }} - {{ $config["max_price"] }}</span>
            </div>
        </div>
        <div class="col-md-3 col-xs-12 col-xs-12 text-center">
            <div class="jbm-job-links">
                <div class="jbm-job-detail">
                    <a href="{{ route("demand.single",["id"=>$config["id"]]) }}">Demand Detail</a>
                </div>
            </div>
        </div>
    </div>
</div>