<!-- end section category -->
<div class="jbm-section-jobs padding-top-80 padding-bottom-100">
    <div class="container">
        <div class="row">
            @foreach($demands as $demand)
                @widget("DemandItem",[
                "logo" => $demand->logo,
                "company_name" => $demand->user->company->name ?? $demand->user->name,
                "city_name" => $demand->country->name ?? "Unknown",
                "title" => $demand->title,
                "min_price" => $demand->min_price,
                "max_price" => $demand->max_price,
                "id" => $demand->id,
                ])
            @endforeach
        </div>
    </div>
</div>