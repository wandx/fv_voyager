@extends("client.master")

@push("on_body")
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-md-5 jbm-login-side">
                        <i class="fa fa-lightbulb-o margin-bottom-50" aria-hidden="true"></i>
                        <span class="section-tit-line"></span>
                        <h3 class="margin-bottom-60">Register</h3>
                        <ul>
                            <li>
                                <a href="#">Already have account? </a>
                            </li>
                            <li>
                                <a href="#">Login Here</a>
                            </li>
                        </ul>

                        <ul class="jbm-social-icons">
                            <li>
                                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                    {!! Form::open(["class"=>"col-md-7 jbm-form"]) !!}
                    @include("client.partials.error")
                    <div class="jbm-field login-style margin-top-60">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input value="{{ old("name") }}" type="text" name="name" id="email-address" class="form-control">
                                <label for="email-address">Name*</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input value="{{ old("email") }}" type="text" name="email" id="email-address" class="form-control">
                                <label for="email-address">Email Address*</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="password" name="password" id="new-pass2" class="form-control">
                                <label for="new-pass2">Enter Password*</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group has-feedback">
                                <input type="password" name="password_confirmation" id="new-pass2" class="form-control">
                                <label for="new-pass2">Password Confirm*</label>
                            </div>
                        </div>

                        <button type="submit" id="btn-submit" class="jbm-button jbm-button-3 jbm-hover margin-bottom-40 margin-top-20" disabled>Register</button>
                        <div class="row margin-bottom-40">
                            <div class="col-md-7">
                                <div class="terms style3">
                                    <input type="checkbox" id="c4" name="cc">
                                    <label for="c4"><span></span></label>
                                    <small>
                                        I have read and agree with Terms & Conditions
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endpush

@push("scripts")
    <script>
        $("#c4").click(function(){
            var btn = $("#btn-submit");
            if($(this).is(":checked")){
                btn.attr("disabled",false);
            }else{
                btn.attr("disabled",true);
            }
        });
    </script>
@endpush