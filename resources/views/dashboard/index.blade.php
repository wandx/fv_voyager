@extends("client.master")

@push("on_body")
    @component("components.startpage")
    @endcomponent

    <div class="jbm-emp-dashboard pad-xs-top-60">
        <div class="container">
            <div class="row margin-bottom-100">
                @widget("DashboardNav",["active"=>"profile-information"])

                <div class="col-md-9 col-sm-12 col-xs-12 pull-right">
                    <div class="job-history">
                        <h4>Profile Information</h4>
                        <span class="section-tit-line-2 margin-bottom-20"></span>
                        <div class="company-details margin-bottom-30 change-pass padding-bottom-60">
                        @include("client.partials.error")
                        <!-- row start -->
                            {!! Form::model(auth()->user(),["class"=>"row","files"=>true]) !!}
                            <div class="col-xs-12">
                                <div class="form-group field-active">
                                    {!! Form::text("name",null,["class"=>"form-control","id"=>"name","required"]) !!}
                                    {!! Form::label("name","Name") !!}
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group field-active">
                                    {!! Form::text("role",null,["class"=>"form-control","id"=>"role"]) !!}
                                    {!! Form::label("role","Role in company") !!}
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group field-active">
                                    {!! Form::textarea("skills",null,["class"=>"form-control","id"=>"skills","rows"=>"1"]) !!}
                                    {!! Form::label("skills","Skills") !!}
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group field-active">
                                    {!! Form::file("avatar_file",["class"=>"form-control","id"=>"avatar_file"]) !!}
                                    {!! Form::label("avatar_file","Avatar") !!}
                                    @if(Storage::exists(auth()->user()->avatar ?? ""))
                                        <span class="form-control-feedback"><img src="{{ Storage::url(auth()->user()->avatar) }}" style="width: 50px;border: 1px solid #ddd;padding: 1px;background: #ddd;" alt=""></span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="text-right">
                                    <button name="submit" type="submit" value="save" class="btn btn-primary">Save</button>
                                </div>

                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endpush