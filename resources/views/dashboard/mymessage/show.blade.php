@extends("master")

@push("on_body")
    @component("components.startpage",["breadcrumb"=>["#"=>"My Message"]])
        @slot("title")
            My Message
        @endslot
    @endcomponent

    <div class="jbm-emp-dashboard pad-xs-top-60">
        <div class="container">
            <div class="row margin-bottom-100">
                @widget("DashboardNav",["active"=>"my-message"])

                <div class="col-md-9 col-sm-12 col-xs-12 pull-right">
                    <div class="job-history">
                        <h4>My Message</h4>
                        <span class="section-tit-line-2 margin-bottom-20"></span>
                        <div class="row">
                            <div class="col-sm-12">
                                @include("partials.error")
                                @include("partials.success")
                                <div id="message-container" style="height: 400px;overflow-y: scroll">
                                @forelse($messages as $message)
                                    @if($message->user_id == auth()->user()->id)
                                        <!-- Left-aligned -->
                                            <div class="media">
                                                <div class="media-left">
                                                    <img src="http://placehold.it/300" class="media-object" style="width:60px">
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading" style="margin-top: 10px">{{ $message->user->name }}</h4>
                                                    <p>{{ $message->body }}</p>
                                                </div>
                                            </div>
                                            <hr>
                                    @else
                                        <!-- Right-aligned -->
                                            <div class="media">
                                                <div class="media-body">
                                                    <h4 class="media-heading" style="margin-top: 10px">{{ $message->user->name }}</h4>
                                                    <p>{{ $message->body }}</p>
                                                </div>
                                                <div class="media-right">
                                                    <img src="http://placehold.it/300" class="media-object" style="width:60px">
                                                </div>
                                            </div>
                                            <hr>
                                        @endif

                                    @empty
                                        <div class="text-center">No Message</div>
                                    @endforelse
                                </div>
                            </div>
                            <form method="post" class="col-sm-12" action="{{ route("mymessage.reply",["message_id"=>$message_id]) }}">
                                <div class="form-group">
                                    {!! csrf_field() !!}
                                    <div class="input-group">
                                        <input name="body" type="text" class="form-control" placeholder="Type your message" required>
                                        <div class="input-group-btn">
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endpush

@push("scripts")
    <script>
        $(function(){
            var element = document.getElementById("message-container");
            element.scrollTop = element.scrollHeight;
        });
    </script>
@endpush