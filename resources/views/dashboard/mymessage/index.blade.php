@extends("master")

@push("on_body")
    @component("components.startpage",["breadcrumb"=>["#"=>"My Message"]])
        @slot("title")
            My Message
        @endslot
    @endcomponent

    <div class="jbm-emp-dashboard pad-xs-top-60">
        <div class="container">
            <div class="row margin-bottom-100">
                @widget("DashboardNav",["active"=>"my-message"])

                <div class="col-md-9 col-sm-12 col-xs-12 pull-right">
                    <div class="job-history">
                        <h4>My Message</h4>
                        <span class="section-tit-line-2 margin-bottom-20"></span>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>From</th>
                                <th>To</th>
                                <th>Message</th>
                                <th>Created</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($messages as $message)
                                <tr>
                                    <td>
                                        @if($message->sender_id == auth()->user()->id)
                                            ME
                                        @else
                                            {{ $message->sender->name ?? "Anonymous" }}
                                        @endif

                                    </td>
                                    <td>
                                        @if($message->receiver_id == auth()->user()->id)
                                            ME
                                        @else
                                            {{ $message->receiver->name ?? "Anonymous" }}
                                        @endif
                                    </td>
                                    <td>{{ $message->message_items()->latest()->first()->body ?? "No Message" }}</td>
                                    <td>
                                        {{ $message->created_at->format("d/M/Y H:i a") }}
                                    </td>
                                    <td>
                                        @if($message->unread_message > 0)
                                            <a href="{{ route("mymessage.show",["message_id"=>$message->id]) }}" class="btn btn-warning btn-xs"><i class="fa fa-envelope"></i> {{ $message->unread_message }}</a>
                                        @else
                                            <a href="{{ route("mymessage.show",["message_id"=>$message->id]) }}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> View</a>
                                        @endif
                                    </td>
                                </tr>
                                @empty
                                    <tr>
                                        <td class="text-center" colspan="3">No Message</td>
                                    </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endpush