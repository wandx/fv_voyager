@extends("master")

@push("on_body")
    @component("components.startpage")
    @endcomponent

    <div class="jbm-emp-dashboard pad-xs-top-60">
        <div class="container">
            <div class="row margin-bottom-100">
                @widget("DashboardNav",["active"=>"my-company"])

                <div class="col-md-9 col-sm-12 col-xs-12 pull-right">
                    <div class="job-history">
                        <h4>My Company</h4>
                        <span class="section-tit-line-2 margin-bottom-20"></span>
                        <div class="company-details margin-bottom-30 change-pass padding-bottom-60">
                        @include("partials.error")
                        @include("partials.success")
                        <!-- row start -->
                            {!! Form::model($company,["class"=>"row","files"=>true]) !!}
                            {!! Form::hidden("company_id",$company->id ?? null) !!}
                            <div class="col-xs-12">
                                <div class="form-group field-active">
                                    {!! Form::text("name",null,["class"=>"form-control","id"=>"name","required"]) !!}
                                    {!! Form::label("name","Name") !!}
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group field-active has-feedback">
                                    {!! Form::text("address",null,["class"=>"form-control","id"=>"address","required"]) !!}
                                    {!! Form::label("address","Address") !!}
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group field-active has-feedback">
                                    {!! Form::text("website",null,["class"=>"form-control","id"=>"website","required"]) !!}
                                    {!! Form::label("website","Website") !!}
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group field-active has-feedback">
                                    {!! Form::textarea("description",null,["class"=>"form-control","id"=>"description","required","placeholder"=>"Describe your company","rows"=>"3"]) !!}
                                    {!! Form::label("description","Description") !!}
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group field-active">
                                    {!! Form::file("logo",["class"=>"form-control","id"=>"logo"]) !!}
                                    {!! Form::label("logo","Logo") !!}
                                    @if(Storage::exists($company->company_logo ?? ""))
                                    <span class="form-control-feedback"><img src="{{ Storage::url($company->company_logo) }}" style="width: 50px;border: 1px solid #ddd;padding: 1px;background: #ddd;" alt=""></span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="text-right">
                                    <button name="submit" type="submit" value="save" class="btn btn-primary">Save</button>
                                </div>

                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endpush