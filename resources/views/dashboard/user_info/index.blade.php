@extends("master")

@push("on_body")
    @component("components.startpage")
    @endcomponent

    <div class="jbm-emp-dashboard pad-xs-top-60">
        <div class="container">
            <div class="row margin-bottom-100">
                @widget("DashboardNav",["active"=>"profile-information"])

                <div class="col-md-9 col-sm-12 col-xs-12 pull-right">
                    <div class="job-history">
                        <h4>Profile Information</h4>
                        <span class="section-tit-line-2 margin-bottom-20"></span>
                        <div class="company-details margin-bottom-30 change-pass padding-bottom-60">
                        @include("partials.error")
                        @include("partials.success")
                        <!-- row start -->
                            {!! Form::model(auth()->user(),["class"=>"row","files"=>true]) !!}
                            <div class="col-xs-12">
                                <div class="form-group field-active">
                                    {!! Form::text("name",null,["class"=>"form-control","id"=>"name","required"]) !!}
                                    {!! Form::label("name","Name") !!}
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group field-active">
                                    {!! Form::textarea("skill",null,["class"=>"form-control","id"=>"skill","rows"=>"1"]) !!}
                                    {!! Form::label("skill","Skill") !!}
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group field-active">
                                    {!! Form::file("avatar_file",["class"=>"form-control","id"=>"avatar_file"]) !!}
                                    {!! Form::label("avatar_file","Avatar") !!}
                                    <span class="form-control-feedback"><img src="{{ Voyager::image(auth()->user()->avatar,"http://placehold.it/300") }}" style="width: 50px;border: 1px solid #ddd;padding: 1px;background: #ddd;" alt=""></span>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="text-right">
                                    <button name="submit" type="submit" value="save" class="btn btn-primary">Save</button>
                                </div>

                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endpush