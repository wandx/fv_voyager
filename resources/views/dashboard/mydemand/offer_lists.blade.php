@extends("master")

@push("on_body")
    @component("components.startpage",["breadcrumb"=>[route("mydemand.index")=>"My Demand","#"=>"Offer list"]])
        @slot("title")
            Offer lists
        @endslot
    @endcomponent

    <div class="jbm-emp-dashboard pad-xs-top-60">
        <div class="container">
            <div class="row margin-bottom-100">
                @widget("DashboardNav",["active"=>"my-demand"])

                <div class="col-md-9 col-sm-12 col-xs-12 pull-right">
                    <div class="jbp-candidate-box">
                        <h4 class="jbm-heading">Offer Lists</h4>
                        <span class="section-tit-line-2 margin-bottom-40"></span>
                    </div><!--jbp-message-box end-->
                    <div class="jbm-canidate-applications">
                        <div class="jbm-candidates style2">
                            @include("partials.error")
                            @include("partials.success")
                            <div class="row">
                                @forelse($offers as $offer)
                                    <div class="col-md-12">
                                        <div class="jbm-candidate-profile">
                                            <div class="pro-img">
                                                <img src="{{ $offer->user->avatar_url }}" alt="">
                                            </div>
                                            <div class="pro-info">
                                                <h5>{{ $offer->user->name }}</h5>
                                                <span>{{ $offer->company->role ?? "no role" }} @ {{ $offer->user->company->name ?? "no company" }}</span>
                                                <span>Offer price: <em>${{ $offer->price }}</em></span>
                                            </div>
                                            <div class="profile-specs">
                                                <ul>
                                                    <li class="">
                                                        <i class="fa fa-lightbulb-o"></i>
                                                        <a href="#" title="">Hire</a>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-envelope"></i>
                                                        <a href="#" onclick="event.preventDefault()" data-toggle="modal" data-target="#myModal" data-receiver="{{ $offer->user->id }}" data-name="{{ $offer->user->name }}" title="">Send Message</a>
                                                    </li>
                                                    <li>
                                                        <i class="fa fa-download"></i>
                                                        <a href="{{ $offer->attachment_link }}" title="">Document</a>
                                                    </li>
                                                </ul>
                                            </div><!--profile-specs end-->
                                            {{--<i class="fa fa-trash-o"></i>--}}
                                            <!-- <i class="fa fa-thrash"></i> -->
                                        </div><!--jbm-candidate-profile end-->
                                    </div>
                                    @empty
                                        <div class="well text-center">No Offers</div>
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endpush

@push("modals")
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            {!! Form::open(["route"=>"mymessage.send_message","class"=>"modal-content"]) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="receiver_id">
                <div class="form-group">
                    <textarea name="body" required id="body" class="form-control" rows="4"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Send</button>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
@endpush

@push("scripts")
    <script>
        $("#myModal").on("show.bs.modal",function(e){
            var trig = $(e.relatedTarget),
                modal = $(this),
                receiver = trig.data("receiver"),
                name = trig.data("name");

            modal.find(".modal-title").html("Send Message To "+name);
            modal.find("input[name=receiver_id]").val(receiver);
        })
    </script>
@endpush