@extends("master")

@push("on_body")
    @component("components.startpage",["breadcrumb"=>[route("mydemand.index")=>"My Demand","#"=>"Create"]])
        @slot("title")
            Create Demand
        @endslot
    @endcomponent

    <div class="jbm-emp-dashboard pad-xs-top-60">
        <div class="container">
            <div class="row margin-bottom-100">
                @widget("DashboardNav",["active"=>"my-demand"])

                <div class="col-md-9 col-sm-12 col-xs-12 pull-right">
                    <div class="job-history">
                        <h4>Create Demand</h4>
                        <span class="section-tit-line-2 margin-bottom-40"></span>

                        <div class="company-details margin-bottom-30 change-pass padding-bottom-60">
                            @include("partials.error")
                            <!-- row start -->
                            {!! Form::open(["class"=>"row","files"=>true]) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group {{ session()->has("active") && session("active") == true ? "field-active":"" }}">
                                        {!! Form::text("title",null,["class"=>"form-control","id"=>"title","required"]) !!}
                                        {!! Form::label("title","Title") !!}
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group {{ session()->has("active") && session("active") == true ? "field-active":"" }} jbm-select-2">
                                        {!! Form::select("country_id",$country,null,["class"=>"jbm-s-salary jbm-select-hide-search","id"=>"jbm-s-business-type","placeholder"=>"Select Country"]) !!}
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group {{ session()->has("active") && session("active") == true ? "field-active":"" }}">
                                        {!! Form::number("min_price",null,["class"=>"form-control","id"=>"min_price","required"]) !!}
                                        {!! Form::label("min_price","Minimum Price") !!}
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group {{ session()->has("active") && session("active") == true ? "field-active":"" }}">
                                        {!! Form::number("max_price",null,["class"=>"form-control","id"=>"max_price","required"]) !!}
                                        {!! Form::label("max_price","Maximum Price") !!}
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group {{ session()->has("active") && session("active") == true ? "field-active":"" }}">
                                        {!! Form::textarea("description",null,["class"=>"form-control","id"=>"description","required","rows"=>"2"]) !!}
                                        {!! Form::label("description","Description") !!}
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group field-active">
                                        {!! Form::file("doc_file",["class"=>"form-control","id"=>"doc","required","style"=>"height:auto"]) !!}
                                        {!! Form::label("doc","Support Document (PDF)") !!}
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="text-right">
                                        <button name="submit" type="submit" value="draft" class="btn btn-primary">Save as Draft</button>
                                        <button name="submit" type="submit" value="publish" class="btn btn-primary">Next: Payment</button>
                                    </div>

                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endpush