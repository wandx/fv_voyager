@extends("master")

@push("on_body")
    @component("components.startpage",["breadcrumb"=>["#"=>"My Demand"]])
        @slot("title")
            My Demand
        @endslot
    @endcomponent

    <div class="jbm-emp-dashboard pad-xs-top-60">
        <div class="container">
            <div class="row margin-bottom-100">
                @widget("DashboardNav",["active"=>"my-demand"])

                <div class="col-md-9 col-sm-12 col-xs-12 pull-right">

                    <div class="job-history">
                        <h4>My Demand</h4>
                        <span class="section-tit-line-2 margin-bottom-20"></span>
                        <div class="text-right margin-bottom-20">
                            @include("partials.error")
                            @include("partials.success")
                            <a href="{{ route("mydemand.create") }}" class="btn btn-primary">Create new demand</a>
                        </div>
                        @widget("MyDemand")
                    </div>
                </div>

            </div>
        </div>
    </div>

@endpush