@extends("master")

@push("on_body")
    @component("components.startpage",["breadcrumb"=>["#"=>"Payment History"]])
        @slot("title")
            Payment History
        @endslot
    @endcomponent

    <div class="jbm-emp-dashboard pad-xs-top-60">
        <div class="container">
            <div class="row margin-bottom-100">
                @widget("DashboardNav",["active"=>"my-payment"])

                <div class="col-md-9 col-sm-12 col-xs-12 pull-right">
                    <div class="job-history">
                        <h4>Payment History</h4>
                        <span class="section-tit-line-2 margin-bottom-20"></span>
                        @widget("MyPayment")
                    </div>
                </div>

            </div>
        </div>
    </div>

@endpush