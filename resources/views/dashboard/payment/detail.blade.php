@extends("master")

@push("on_body")
    @component("components.startpage",["breadcrumb"=>[route("mypayment.index")=>"Payment History","#"=>$transaction->invoice_no]])
        @slot("title")
            Payment Detail
        @endslot
    @endcomponent

    <div class="jbm-emp-dashboard pad-xs-top-60">
        <div class="container">
            <div class="row margin-bottom-100">
                @widget("DashboardNav",["active"=>"my-demand"])

                <div class="col-md-9 col-sm-12 col-xs-12 pull-right">
                    <div class="row">
                        <div class="col-sm-12">
                            @include("partials.error")
                            @include("partials.success")
                            <h3>Invoice: {{ $transaction->invoice_no }}</h3>

                            <table class="table">
                                <tbody>
                                <tr>
                                    <th style="width: 10%">Related service</th>
                                    <td style="width: 2%;">:</td>
                                    @if($transaction->payment_for == "offer")
                                        <td><a target="_blank" href="{{ route("demand.single",["id"=>$transaction->related->demand->id]) }}">{{ $transaction->related->demand->title}}</a></td>
                                    @else
                                        <td><a target="_blank" href="{{ route("demand.single",["id"=>$transaction->related]) }}">{{ $transaction->related->title }}</a></td>
                                    @endif
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>:</td>
                                    <td>{{ $transaction->status }}</td>
                                </tr>
                                <tr>
                                    <th>Amount</th>
                                    <td>:</td>
                                    <td>{{ $transaction->amount }} USD</td>
                                </tr>
                                <tr>
                                    <th>Description</th>
                                    <td>:</td>
                                    <td>{{ $transaction->description}}</td>
                                </tr>
                                <tr>
                                    <th>Message</th>
                                    <td>:</td>
                                    <td>{{ $transaction->message ?? "-"}}</td>
                                </tr>
                                </tbody>
                            </table>
                            @if($transaction->status != "paid")
                            <div class="text-center">
                                <label for="agree" class="checkbox">
                                    <input type="checkbox" name="agree" value="agree"> I agree
                                </label>

                                <a href="{{ route("pay_paypal",["order_id"=>$transaction->id]) }}" class="btn btn-primary">PAY</a>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endpush