@extends("client.master")

@push("on_body")
    @component("client.components.startpage")
    @endcomponent

    <div class="jbm-emp-dashboard pad-xs-top-60">
        <div class="container">
            <div class="row margin-bottom-100">
                @widget("DashboardNav",["active"=>"my-offer"])

                <div class="col-md-9 col-sm-12 col-xs-12 pull-right">
                    <div class="job-history">
                        <h4>Create Offer</h4>
                        <span class="section-tit-line-2 margin-bottom-40"></span>

                        <div class="company-details margin-bottom-30 change-pass padding-bottom-60">
                            <h5 class="margin-bottom-60">Company Details</h5>
                            <!-- row start -->
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <input type="text" name="com-name" id="com-name2" class="form-control">
                                        <label for="com-name2">Company Name*</label>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group jbm-select-2">
                                        <select class="jbm-s-salary jbm-select-hide-search" name="business-type" id="jbm-s-business-type">
                                            <option value="">Business Type*</option>
                                            <option value="individual">Individual</option>
                                            <option value="company">Company</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endpush