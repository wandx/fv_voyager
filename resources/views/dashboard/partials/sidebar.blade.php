<!-- Emp sidebar -->
<div class="col-md-3 col-sm-12 col-xs-12">
    <div class="jbm-emp-sidebar padding-bottom-30 padding-top-30">
        <ul class="jbm-dashboard-links">
            <li>
                <a href="candidate-information.html" class="active">Profile Informations</a>
            </li>
            <li>
                <a href="candidate-message.html">Message</a>
            </li>
            <li>
                <a href="resume.html">Resume</a>
            </li>
            <li>
                <a href="candidate-job-history.html">Job History</a>
            </li>
            <li>
                <a href="cv.html">CV and Cover Letter</a>
            </li>
            <li>
                <a href="candidate-account-setting.html">Account Settings</a>
            </li>
            <li>
                <a href="/logout">Logout</a>
            </li>
        </ul>
    </div>
</div>