@extends("master")

@push("on_body")
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-md-5 jbm-login-side">
                        <i class="fa fa-lightbulb-o margin-bottom-50" aria-hidden="true"></i>
                        <span class="section-tit-line"></span>
                        <h3 class="margin-bottom-60">Login</h3>
                        <ul>
                            <li>
                                <a href="#">Not yet registered? </a>
                            </li>
                            <li>
                                <a href="#">Register Here</a>
                            </li>
                        </ul>

                        <ul class="jbm-social-icons">
                            <li>
                                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                    {!! Form::open(["class"=>"col-md-7 jbm-form"]) !!}
                        @include("partials.error")
                        <div class="jbm-field login-style margin-top-60">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="email" id="email-address" class="form-control">
                                    <label for="email-address">Email Address*</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="password" name="password" id="new-pass2" class="form-control">
                                    <label for="new-pass2">Enter Password*</label>
                                </div>
                            </div>
                            <button type="submit" class="jbm-button jbm-button-3 jbm-hover margin-bottom-40 margin-top-20">Login</button>
                            <div class="row margin-bottom-40">
                                <div class="col-md-7">
                                    <div class="terms style3">
                                        <input type="checkbox" id="c4" name="cc">
                                        <label for="c4"><span></span></label>
                                        <small>Keep me logged in</small>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <a href="#" class="forgot">Forgot Password?</a>
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endpush