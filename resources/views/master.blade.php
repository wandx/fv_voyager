<!doctype html>
<html lang="en">


<!-- Mirrored from demo.diothemes.com/html/jobmarket/index2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 24 Jun 2018 10:28:53 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Fv</title>
    <!-- stylesheets-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    @stack("on_header")
</head>

<body>
<header>
    @include("partials.topbar")
    @include("partials.navigation")
    @include("partials.navigation_mobile")
</header>
@stack("on_body")
@include("partials.footer")



<div class="jbm-overlay jbm-2nd-bg"></div>
@stack("modals")
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
@stack("scripts")
</body>
</html>