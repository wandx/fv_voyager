@extends("master")

@push("on_body")
    @include("partials.start_banner")
    @widget("HomeSearch")
    {{--<br>--}}
    <br>
    @include("partials.callout")
    @include("partials.latest_job_title")
    @widget("LatestJob")
    @include("partials.helpbox")
    @widget("Partners")
    {{--@include("partials.partners")--}}
    @include("partials.warranty")

@endpush

